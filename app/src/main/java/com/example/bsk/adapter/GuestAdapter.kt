package com.example.bsk.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bsk.data.Guest
import com.example.bsk.R


class GuestAdapter(var data: List<Guest>) : RecyclerView.Adapter<GuestAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textName.text = data[position].name
        Glide.with(holder.itemView.getContext()).load(data[position].avatar).into(holder.icon)
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val icon = itemView.findViewById<ImageView>(R.id.icon)
        val textName = itemView.findViewById<TextView>(R.id.text_name)

    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun addItems(guests: List<Guest>){
        data.apply {
            data = guests
            notifyDataSetChanged()
        }
    }

}


