package com.example.bsk.data

import com.google.gson.annotations.SerializedName

data class GuestResponse(
    @SerializedName("guests") val guests: List<Guest>
)