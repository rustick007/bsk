package com.example.bsk.presentation.information

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bsk.R

import com.example.bsk.adapter.GuestAdapter
import com.example.bsk.data.Guest
import com.example.bsk.presentation.JsonReader
import com.example.bsk.presentation.loadDrawableImage
import com.example.bsk.presentation.loadUrlImage
import kotlinx.android.synthetic.main.activity_main.*

class InformActivity : AppCompatActivity(), InformViewMvp.View {

    companion object {
        const val JSON_FILE_NAME = "guestlist.json"
    }

    private val presenter: InformViewMvp.Presenter by lazy { InformPresenter() }
    private val gusetsAdapter: GuestAdapter by lazy { GuestAdapter(emptyList<Guest>()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.init(this)
        topAppBar.setNavigationOnClickListener { presenter.navigateBackClick() }
        recycler_view.adapter = gusetsAdapter
        val jsonFileString = JsonReader.getJsonDataFromAsset(this, JSON_FILE_NAME)
        presenter.validateJsonString(jsonFileString)
    }

    override fun initViews() {
        photo_view.loadDrawableImage(R.drawable.ic_bocal1, R.drawable.ic_bocal1)
        imageView.loadUrlImage("https://reqres.in/img/faces/3-image.jpg")
    }

    override fun showGuests(guests: List<Guest>) {
        gusetsAdapter?.addItems(guests)
    }

    override fun navigateBack() {
        Toast.makeText(this,R.string.toast_message, Toast.LENGTH_SHORT)
            .show()
    }
}