package com.example.bsk.presentation.information

import com.example.bsk.data.GuestResponse
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class InformPresenter: InformViewMvp.Presenter {

    private lateinit var view: InformViewMvp.View

    override fun init(viewMvp: InformViewMvp.View) {
        this.view = viewMvp
        viewMvp.initViews()
    }

    override fun validateJsonString(jsonString: String?) {
        jsonString?.apply {
            val type = object : TypeToken<GuestResponse>() {}.type
            val guestResponse: GuestResponse = Gson().fromJson(this, type)
            view.showGuests(guestResponse.guests)
        }
    }

    override fun navigateBackClick() {
        view.navigateBack()
    }
}