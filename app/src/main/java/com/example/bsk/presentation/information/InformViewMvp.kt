package com.example.bsk.presentation.information

import com.example.bsk.data.Guest


interface InformViewMvp {
    interface View {
        fun showGuests(guests: List<Guest>)
        fun navigateBack()
        fun initViews()
    }

    interface Presenter {
        fun init(viewMvp: InformViewMvp.View)
        fun navigateBackClick()
        fun validateJsonString(jsonString: String?)
    }
}