package com.example.bsk.presentation

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.loadUrlImage(url: String){
    Glide.with(this.context)
        .load(url)
        .into(this)
}

fun ImageView.loadDrawableImage(drawable: Int, placeHolder: Int){
    Glide.with(this.context)
        .load(drawable)
        .placeholder(placeHolder)
        .into(this)
}